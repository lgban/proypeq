package com.example.demo.model;

import java.util.List;

public interface AdvancedQueryInterface {
    List<PlantInventoryEntry> metodoDeConsulta();
    List<PlantInventoryEntry> recuperaTodasLasPlantas();
}
