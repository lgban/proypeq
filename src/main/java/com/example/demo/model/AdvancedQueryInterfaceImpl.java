package com.example.demo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

@Service
public class AdvancedQueryInterfaceImpl implements AdvancedQueryInterface {
    @Autowired
    DataSource dataSource;

    @Autowired
    EntityManager em;

    @Override
    public List<PlantInventoryEntry> metodoDeConsulta() {
        System.out.println("Hola, estoy en el metodo extendido");
        try {
            Statement stmt = dataSource.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from PlantInventoryEntry");
            while (rs.next()) {
                String name = rs.getString("name");
                System.out.println(name);
            }
        } catch (Exception e) {}
        return null;
    }

    public List<PlantInventoryEntry> recuperaTodasLasPlantas() {
        return
            em.createQuery("select p from PlantInventoryEntry p", PlantInventoryEntry.class)
                .getResultList();
    }
}
